import { Component, OnInit } from '@angular/core';
import { PqrService } from 'src/app/services/pqr.service';

@Component({
  selector: 'app-form-pqr',
  templateUrl: './form-pqr.component.html',
  styleUrls: ['./form-pqr.component.scss'],
})
export class FormPqrComponent implements OnInit {
  
  pqr = {
    requestType: '',
    documentType: '',
    documentNumber: '',
    requestDetail: '',
  };
  submitted: boolean = false;

  constructor(private pqrService: PqrService) { }

  ngOnInit(): void { }

  createPqr(): void {
    const data = {
      requestType: this.pqr.requestType,
      documentType: this.pqr.documentType,
      documentNumber: this.pqr.documentNumber,
      requestDetail: this.pqr.requestDetail,
    };

    this.pqrService.create(data).subscribe(
      (response) => {
        console.log(response);
        this.submitted = true;
        setTimeout(()=>{                      
          this.submitted = false;
        }, 10000);
      },
      (error) => {
        this.submitted = false;
        console.log(error);
      }
    );
  }
}
