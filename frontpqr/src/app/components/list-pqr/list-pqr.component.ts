import { Component, OnInit } from '@angular/core';
import { PqrService } from 'src/app/services/pqr.service';

@Component({
  selector: 'app-list-pqr',
  templateUrl: './list-pqr.component.html',
  styleUrls: ['./list-pqr.component.scss']
})
export class ListPqrComponent implements OnInit {

  pqrs: any;

  constructor(private pqrService: PqrService) { }

  ngOnInit(): void {
    this.readPqrs();
  }

  readPqrs(): void {
    this.pqrService.getAll()
      .subscribe(
        pqrs => {
          this.pqrs = pqrs;
          console.log(pqrs);
        },
        error => {
          console.log(error);
        });
  }
}
