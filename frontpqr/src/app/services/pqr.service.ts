import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Pqr } from '../models/pqr.model';

const baseUrl = 'http://localhost:8080/api/pqr';

@Injectable({
  providedIn: 'root'
})
export class PqrService {

  constructor(private http: HttpClient) { }

  getAll(): Observable<Pqr[]> {
    return this.http.get<Pqr[]>(baseUrl);
  }

  create(data: any): Observable<any> {
    return this.http.post(baseUrl, data);
  }
}
