package co.com.backpqr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackpqrApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackpqrApplication.class, args);
	}

}
