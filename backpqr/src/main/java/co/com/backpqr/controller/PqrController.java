package co.com.backpqr.controller;

import co.com.backpqr.model.PqrModel;
import co.com.backpqr.repository.PqrRespository;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class PqrController {

    @Autowired
    PqrRespository pqrRespository;

    @GetMapping("/pqr")
    public Collection<PqrModel> getAllPqrs() {
        return pqrRespository.findAll();
    }

    @PostMapping("/pqr")
    public PqrModel createPqr(@PathVariable("id") String id, @RequestBody PqrModel pqrModel) {
        return pqrRespository.insert(pqrModel);
    }
}
