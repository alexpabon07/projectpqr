package co.com.backpqr.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "pqr")
public class PqrModel {
    @Id
    private String id;

    private String requestType;
    private String documentType;
    private String documentNumber;
    private String requestDetail;

    public PqrModel(String requestType, String documentType, String documentNumber, String requestDetail) {
        this.requestType = requestType;
        this.documentType = documentType;
        this.documentNumber = documentNumber;
        this.requestDetail = requestDetail;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getRequestDetail() {
        return requestDetail;
    }

    public void setRequestDetail(String requestDetail) {
        this.requestDetail = requestDetail;
    }
}
