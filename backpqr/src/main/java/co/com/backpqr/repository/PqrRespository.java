package co.com.backpqr.repository;

import co.com.backpqr.model.PqrModel;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface PqrRespository extends MongoRepository<PqrModel, String> {

}
